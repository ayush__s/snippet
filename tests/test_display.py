# -*- coding: utf-8 -*-
from mock import MagicMock

import json
from os import path
from requests import Response
from pybitbucket.snippet import Snippet
from pybitbucket.user import User
from pybitbucket.comment import Comment
from pybitbucket.commit import Commit
from pybitbucket.bitbucket import Client, ServerError
from pybitbucket.auth import Authenticator

from snippet import display


class TestAuth(Authenticator):

    def get_username(self):
        return self.username

    def __init__(self):
        self.server_base_uri = 'https://staging.bitbucket.org/api'
        self.username = 'pybitbucket'
        self.password = 'secret'
        self.email = 'pybitbucket@mailinator.com'
        self.session = self.start_http_session()


class TestDisplay(object):
    @classmethod
    def setup_class(cls):
        cls.test_dir, current_file = path.split(path.abspath(__file__))
        cls.client = Client(TestAuth())

    def load_example_snippet(self):
        example_path = path.join(
            self.test_dir,
            'example_single_snippet.json')
        with open(example_path) as f:
            example = json.load(f)
        return Snippet(example, client=self.client)

    def load_example_user(self):
        example_path = path.join(
            self.test_dir,
            'example_single_user.json')
        with open(example_path) as f:
            example = json.load(f)
        return User(example, client=self.client)

    def load_example_comment(self):
        example_path = path.join(
            self.test_dir,
            'example_single_comment.json')
        with open(example_path) as f:
            example = json.load(f)
        return Comment(example, client=self.client)

    def load_example_commit(self):
        example_path = path.join(
            self.test_dir,
            'example_single_commit.json')
        with open(example_path) as f:
            example = json.load(f)
        return Commit(example, client=self.client)

    def test_show_command(self, capsys):
        display.show_command('fail')
        out, err = capsys.readouterr()
        assert 'fail None\n' == out

    def test_format_user(self):
        u_str = display.format_user(self.load_example_user())
        assert 'Erik van Zijst <evzijst>' == u_str

    def test_show_user(self, capsys):
        display.show_user(self.load_example_user())
        out, err = capsys.readouterr()
        assert 'Erik van Zijst <evzijst>\n' == out

    def test_show_details(self, capsys):
        display.show_details(self.load_example_snippet())
        out, err = capsys.readouterr()
        assert out.startswith('id          : Xqoz8\n')

    def test_show_line(self, capsys):
        display.show_line(self.load_example_snippet())
        out, err = capsys.readouterr()
        assert 'Xqoz8 - Test Snippet\n' == out

    def test_show_comment(self, capsys):
        display.show_comment(self.load_example_comment())
        out, err = capsys.readouterr()
        assert out.startswith('Author:')

    def test_show_commit(self, capsys):
        display.show_commit(self.load_example_commit())
        out, err = capsys.readouterr()
        assert out.startswith('commit')

    def test_show_server_error(self, capsys):
        r = type('r', (Response,), {
            'url': 'https://staging.bitbucket.org/api',
            'status_code': 500,
            'text': ''
        })
        r.json = MagicMock(return_value=None, side_effect=ValueError())
        e = ServerError(r)
        display.show_server_error(e)
        out, err = capsys.readouterr()
        assert out.startswith('Oops!')
