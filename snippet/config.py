# -*- coding: utf-8 -*-
import json
from os import getenv, path, makedirs

from snippet import metadata
from pybitbucket.auth import BasicAuthenticator


class CommandLineConfig(BasicAuthenticator):
    @staticmethod
    def bitbucket_url():
        return getenv('BITBUCKET_URL', 'https://api.bitbucket.org')

    @staticmethod
    def config_file(
            basedir='~',
            appdir='.' + metadata.package,
            filename='bitbucket.json'):
        config_path = path.expanduser(path.join(basedir, appdir))
        return path.join(config_path, filename)

    @staticmethod
    def load_all_configs(config_file=None):
        if not config_file:
            config_file = CommandLineConfig.config_file()
        all_cfgs = dict()
        if path.isfile(config_file):
            with open(config_file, 'r') as f:
                all_cfgs = json.load(f)
        return all_cfgs

    @staticmethod
    def load_config(
            config_file=None,
            bitbucket_url=None):
        if not bitbucket_url:
            bitbucket_url = CommandLineConfig.bitbucket_url()
        if not config_file:
            config_file = CommandLineConfig.config_file()
        d = {
            'server_base_uri': 'https://api.bitbucket.org',
            'username': 'pybitbucket',
            'password': 'secret',
            'client_email': 'pybitbucket@mailinator.com'
        }
        all_cfgs = CommandLineConfig.load_all_configs(config_file=config_file)
        if all_cfgs.get(bitbucket_url):
            cfg = all_cfgs[bitbucket_url]
            d['server_base_uri'] = bitbucket_url
            d['client_email'] = cfg['email']
            d['username'] = cfg['auth']['HTTPBasicAuth']['username']
            d['password'] = cfg['auth']['HTTPBasicAuth']['password']
        return d

    @staticmethod
    def save_config(
            username,
            password,
            email,
            config_file=None,
            bitbucket_url=None):
        if not bitbucket_url:
            bitbucket_url = CommandLineConfig.bitbucket_url()
        if not config_file:
            config_file = CommandLineConfig.config_file()
        d = {
            'email': email,
            'auth': {
                'HTTPBasicAuth': {
                    'username': username,
                    'password': password
                }
            }
        }
        try:
            makedirs(path.dirname(config_file))
        except OSError:
            if not path.isdir(path.dirname(config_file)):
                raise
        with open(config_file, 'a+') as f:
            all_cfgs = dict()
            if path.getsize(config_file) > 0:
                all_cfgs = json.load(f)
            all_cfgs[bitbucket_url] = d
            json.dump(all_cfgs, f)

    def __init__(
            self,
            config_file=None,
            bitbucket_url=None):
        if not bitbucket_url:
            bitbucket_url = CommandLineConfig.bitbucket_url()
        if not config_file:
            config_file = CommandLineConfig.config_file()
        d = CommandLineConfig.load_config(
            config_file=config_file,
            bitbucket_url=bitbucket_url)
        self.__dict__ = d
        self.session = self.start_http_session()
