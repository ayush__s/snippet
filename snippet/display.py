def show_command(cmd, c_args=None):
    s = "{cmd} {c_args}".format(
        cmd=cmd,
        c_args=c_args,
        )
    print(s)


def format_user(u):
    return "{} <{}>".format(u.display_name, u.username)


def show_details(snippet):
    s = '\n'.join([
        "id          : {}".format(snippet.id),
        "is_private  : {}".format(snippet.is_private),
        "title       : {}".format(snippet.title),
        "files       : {}".format(snippet.filenames),
        "creator     : {}".format(format_user(snippet.creator)),
        "created_on  : {}".format(snippet.created_on),
        "owner       : {}".format(format_user(snippet.owner)),
        "updated_on  : {}".format(snippet.updated_on),
        "scm         : {}".format(snippet.scm),
        ]) if snippet else 'Snippet not found.'
    print(s)


def show_line(snippet):
    private = '-' if snippet.is_private else '+'
    title = snippet.title if snippet.title else '<untitled>'
    s = "{id} {private} {title}".format(
        id=snippet.id,
        private=private,
        title=title,
        )
    print(s)


def show_config(url, values):
    s = "{username}@{url}".format(
        url=url,
        username=values['auth']['HTTPBasicAuth']['username'],
        )
    print(s)


def show_filename(f):
    print(f)


def show_content(c):
    print(c)


def show_user(u):
    s = format_user(u)
    print(s)


def show_comment(c):
    s = """Author: {user}
Date:   {created_on}

    {content}
""".format(
        user=format_user(c.user),
        created_on=c.created_on,
        content=c.content.get('raw'),
        )
    print(s)


def show_commit(c):
    s = """commit {hash}
Author: {user}
Date:   {date}

    {message}
""".format(
        hash=c.hash,
        user=format_user(c.author),
        date=c.date,
        message=c.message,
        )
    print(s)


def show_server_error(e):
    s = """Oops! Bitbucket seems to be having a problem.
For more information check the Bitbucket status page:
http://status.bitbucket.org/

{}""".format(e.message)
    print(s)
